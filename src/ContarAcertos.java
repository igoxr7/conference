import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class ContarAcertos {
	private static Map<Integer, List<String>> summary = new HashMap<>();
	private static List<Set<Integer>> volantes = new ArrayList<>();
	private static Map<Integer, BigDecimal> premiacao;
	
	static {
		premiacao = new TreeMap<>();
		premiacao.put(11, new BigDecimal(4.00));
		premiacao.put(12, new BigDecimal(8.00));
		premiacao.put(13, new BigDecimal(20.00));
		premiacao.put(14, new BigDecimal(20000.00));
		premiacao.put(15, new BigDecimal(100000.00));
	}
	
	private static Integer[] resultado = new Integer[] { 03, 06, 07, 10, 11, 12, 13, 14, 15, 18, 19, 20, 21, 24, 25 };

	private static int printAcertoGOE = 11;
	private static boolean printDetail = false;

	public static void main(String[] args) throws Exception {
		String filepath = "./resources/volantes.txt";
		importVolantes(filepath);
		conference(new TreeSet(Arrays.asList(ContarAcertos.resultado)), ContarAcertos.volantes);
		printSummary();
	}

	private static void importVolantes(String filepath) throws IOException {
		String readLine = "";
		File f = new File(filepath);

		BufferedReader b = new BufferedReader(new FileReader(f));
		while ((readLine = b.readLine()) != null) {
			String balls[] = readLine.split(" ");
			Set<Integer> volante = new TreeSet(Arrays.asList(balls).stream().map(ball -> {
				return Integer.valueOf(ball);
			}).collect(Collectors.toSet()));
			ContarAcertos.volantes.add(volante);
		}
	}

	private static void conference(Set<Integer> results, List<Set<Integer>> volantes) {
		if (results == null || results.size() != 15) {
			throw new RuntimeException("Results list invalid!");
		}

		int v = 1;

		for (Set<Integer> volante : volantes) {
			StringBuilder sb = new StringBuilder();
			sb.append(String.format("Volante %03d: ", v));

			int acertos = 0;
			for (Integer dozen : volante) {
				if (results.contains(dozen)) {
					acertos++;
				}
				String mask;
				if (results.contains(dozen)) {
					mask = "(%02d), ";
				} else {
					mask = "  %02d, ";
				}
				sb.append(String.format(mask, dozen));
			}

			ContarAcertos.fillSummary(acertos, sb.toString());

			v++;
		}
	}

	private static void fillSummary(int key, String volante) {
		List<String> list;
		if (!summary.containsKey(key)) {
			list = new ArrayList<>();
		} else {
			list = summary.get(key);
		}
		list.add(volante);

		summary.put(key, list);
	}

	private static void printSummary() {
		BigDecimal total = new BigDecimal(0);
		for (Entry<Integer, List<String>> entry : ContarAcertos.summary.entrySet()) {
			int key = entry.getKey();
			if (ContarAcertos.printAcertoGOE < 0 || ContarAcertos.printAcertoGOE <= key) {
				BigDecimal parcial = ContarAcertos.premiacao.get(key).multiply(new BigDecimal(key));
				total = total.add(parcial);
				System.out.println(String.format("%02d acertos: %d ..: %15s", key, entry.getValue().size(), String.format("R$ %.2f", parcial)));
				if (printDetail) {
					System.out.println(
							"------------------------------------------------------------------------------------------------------");
					for (String volante : entry.getValue()) {
						System.out.println(volante);
					}
					System.out.println();
				}
			}
		}
		System.out.println(String.format("Ganhamos ........: %15s", String.format("R$ %.2f", total)));
		System.out.println(String.format("Para cada um ....: %15s",
				String.format("R$ %.2f", total.divide(new BigDecimal(7)).setScale(2))));
	}

}
